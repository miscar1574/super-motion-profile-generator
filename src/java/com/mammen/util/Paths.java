package com.mammen.util;

public class Paths {

    /** this class contains global paths to different support files in the program
     * make sure to include the path from the source folder
     * for better organization you may save the files in the supportFiles directory
     *
     * watch out!
     * some of the fxml files do not use this class so the paths have to be changed at this files too
     * those files are:
     *  - AboutDialog.fxml -> image
     */
    
    public final static String darkMode = "/com/mammen/util/supportFiles/darkTheme.css";

    public final static String sshKeyGenerationInstructions = "src/java/com/mammen/util/supportFiles/SSHinstructions.txt";
    
    public final static String changeLog = "CHANGELOG.md";
}

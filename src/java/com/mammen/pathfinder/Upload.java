package com.mammen.pathfinder;

import com.mammen.ssh.Path;
import com.mammen.ssh.ssh;

public class Upload {

    private String targetIP = "192.168.14.102";      //the target ip to upload
    private Path targetPath = new Path("Documents"); //the directory on target to upload to
    private Path quickUpload = null;                 //the local working path

    final private String USERNAME = "pi";            //the username on target
    final private String FILETYPE = ".csv";          //the file type to upload
    final private ssh connection = new ssh();        //the ssh & scp connection between devices

    //the editable values are updated from the the settings at startup and at every change

    /* upload the file to the target using SCP protocol */
    public String uploadToTarget(String stringPath) throws UploadException, ssh.sshException {
        if(stringPath == "") throw new UploadException("no file");

        Path localPath = new Path(stringPath);
        Path uploadPath = new Path(targetPath);
        uploadPath.addFileName(localPath.getShortName(FILETYPE));

        connection.establishConnection(targetIP, USERNAME);
        connection.scpCommand(localPath.getFullPath(FILETYPE),uploadPath.getFullPath(FILETYPE));
        connection.closeConnection();

        setQuickUpload(localPath);
        return String.format("uploaded %s successfully to %s on %s",stringPath,uploadPath.getFullPath(FILETYPE),targetIP);

    }

    /* update the target ip */
    public void setTarget(String target) {
        this.targetIP = target;
    }

    /* update the path to save files in target */
    public void setTargetPath(String targetPath) {
        this.targetPath = new Path(targetPath);
    }

    /* Thrown when a csv file cant be uploaded to the robot */
    public static class UploadException extends Exception {
        public UploadException(String message) {
            super(message);
        }
    }

    /*set the quickUpload File*/
    private void setQuickUpload(Path localPath){
        this.quickUpload = new Path(localPath);
    }

    /*get the quickUpload file local path*/
    public String getQuickUpload(){
        if(quickUpload == null) return null;
        return this.quickUpload.getFullPath(".csv");
    }

    /*get the quickUpload file name*/
    public String getQuickUploadName(){
        if(quickUpload == null) return null;
        return this.quickUpload.getShortName(".csv");
    }

    /*reset quick upload file*/
    public void resetQuickUpload(){
        this.quickUpload = null;
    }

}

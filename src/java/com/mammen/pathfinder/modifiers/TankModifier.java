package com.mammen.pathfinder.modifiers;

import com.mammen.pathfinder.PathfinderJNI;
import com.mammen.pathfinder.Trajectory;

import java.util.ArrayList;
import java.util.List;

/**
 * The Tank Modifier will take in a Source Trajectory and a Wheelbase Width and spit out a Trajectory for each
 * side of the wheelbase. This is commonly used in robotics for robots which have a drive system similar
 * to a 'tank', where individual parallel sides are driven independently
 *
 * The Source Trajectory is measured from the centre of the drive base. The modification will not modify the central
 * trajectory
 *
 * @author Jaci
 */
public class TankModifier {

    private Trajectory source, left, right;

    /**
     * Create an instance of the modifier
     * @param source The source (center) trajectory
     */
    public TankModifier(Trajectory source) {
        this.source = source;
    }

    /**
     * Generate the Trajectory Modification
     * @param wheelbase_width   The width (in meters) between the individual sides of the drivebase
     * @return                  self
     */
    public TankModifier modify(double wheelbase_width) {
        double w = wheelbase_width / 2;
        Trajectory.Segment[] seg = source.segments;

        List<Trajectory.Segment> left_seg = new ArrayList<>();
        List<Trajectory.Segment> right_seg = new ArrayList<>();

        Trajectory.Segment lastR = seg[0].copy();
        Trajectory.Segment lastL = seg[0].copy();

        boolean first = true;
        for (Trajectory.Segment s : seg) {
            Trajectory.Segment left = s.copy();
            Trajectory.Segment right = s.copy();

            double cos_angle = Math.cos(s.heading);
            double sin_angle = Math.sin(s.heading);

            right.x = s.x + (w * sin_angle);
            right.y = s.y - (w * cos_angle);
            left.x = s.x - (w * sin_angle);
            left.y = s.y + (w * cos_angle);

            if (!first) {
                double distanceL = Math.sqrt(Math.pow(left.x - lastL.x, 2) + Math.pow(left.y - lastL.y, 2));
                double distanceR = Math.sqrt(Math.pow(right.x - lastR.x, 2) + Math.pow(right.y - lastR.y, 2));

                right.position = lastR.position + distanceR;
                right.velocity = distanceR / s.dt;
                right.acceleration = (right.velocity - lastR.velocity) / s.dt;
                right.jerk = (right.acceleration - lastR.acceleration) / s.dt;

                left.position = lastL.position + distanceL;
                left.velocity = distanceL / s.dt;
                left.acceleration = (left.velocity - lastL.velocity) / s.dt;
                left.jerk = (left.acceleration - lastL.acceleration) / s.dt;
            }

            right_seg.add(right);
            left_seg.add(left);

            lastR = right.copy();
            lastL = left.copy();
            first = false;
        }

        this.right = new Trajectory(right_seg.toArray(source.segments.clone()));
        this.left = new Trajectory(left_seg.toArray(source.segments.clone()));

        return this;
    }

    /**
     * Get the initial source trajectory
     * @return the source trajectory
     */
    public Trajectory getSourceTrajectory() {
        return source;
    }

    /**
     * Get the trajectory for the left side of the drive base
     * @return a trajectory for the left side
     */
    public Trajectory getLeftTrajectory() {
        return left;
    }

    /**
     * Get the trajectory for the right side of the drive base
     * @return a trajectory for the right side
     */
    public Trajectory getRightTrajectory() {
        return right;
    }

}

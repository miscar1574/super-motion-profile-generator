package com.mammen.pathfinder;

public class Spline {
    public static class Coord {
        public double x, y;

        public  Coord(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    //TODO: add comments

    double a, b, c, d, e;

    double x_offset, y_offset, angle_offset;
    double knot_distance, arc_length;

    Coord splineCoords(double percenatge) {
        percenatge = range(percenatge);

        double x = percenatge * knot_distance;
        double y = (a*x + b) * Math.pow(x, 4) + (c*x + d) * Math.pow(x, 2) + e*x;

        double cos_theta = Math.cos(angle_offset);
        double sin_theta = Math.sin(angle_offset);
        Coord c = new Coord(
                x * cos_theta - y * sin_theta + x_offset,
                x * sin_theta + y * cos_theta + y_offset);

        return c;
    }

    double splineDeriv(double percentage) {
        percentage = range(percentage);
        double x = percentage * knot_distance;

        return (5*a*x + 4*b) * Math.pow(x, 3) + (3*c*x + 2*d) * x + e;
    }

    double splineAngle(double percentage) {
        return Math.atan(splineDeriv(percentage)) + angle_offset;
    }

    double splineDistance(int sample_count) {
        double deriv0 = splineDeriv(0);
        double last_integrand = Math.sqrt(1.0 + Math.pow(deriv0, 2)) / sample_count;

        double t, deriv, integrand, arc_length;
        arc_length = 0;

        for (int i = 0; i < sample_count; i++) {
            t = (double)i / (double)sample_count;
            deriv = splineDeriv(t);
            integrand = Math.sqrt(1 + Math.pow(deriv, 2)) / sample_count;
            arc_length += (integrand + last_integrand) / 2;
            last_integrand = integrand;
        }

        this.arc_length = knot_distance * arc_length;
        return this.arc_length;
    }

    double splineProgressForDistance(double distance, int sample_count) {
        double deriv0 = splineDeriv(0);
        double last_integrand = Math.sqrt(1.0 + Math.pow(deriv0, 2)) / sample_count;

        double t, deriv, integrand, arc_length, last_arc_length;
        t = arc_length = last_arc_length = 0;

        distance /= this.arc_length;

//        for (int i = 0; i < sample_count; i++) {
//            t = (double)i / (double)sample_count;
//            deriv = splineDeriv(t);
//            integrand = Math.sqrt(1 + Math.pow(deriv, 2)) / sample_count;
//            arc_length += (integrand + last_integrand) / 2;
//            if (arc_length > distance)
//                break;
//            last_integrand = integrand;
//            last_arc_length = arc_length;
//        }
//
//        double interpolated = t;
//        if (arc_length != last_arc_length)
//            interpolated += ((distance - last_arc_length) / (arc_length - last_arc_length) - 1) / sample_count;

        return distance;
    }

    private double range(double value) {
        return value > 1 ? 1 : (value < 0 ? 0 : value);
    }

}

package com.mammen.pathfinder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * The main class of the Pathfinder Library. The Pathfinder Library is used for Motion Profile and Trajectory Generation.
 *
 * This class contains some of the most common methods you will use when doing Motion Profiling
 *
 * @author Jaci
 */
public class Pathfinder {

    /**
     * Convert degrees to radians. This is included here for static imports. In this library, all angle values are
     * given in radians
     * @param degrees the degrees input
     * @return the input in radians
     */
    public static double d2r(double degrees) {
        return Math.toRadians(degrees);
    }

    /**
     * Convert radians to degrees. This is included here for static imports.
     * @param radians the radians input
     * @return the input in degrees
     */
    public static double r2d(double radians) {
        return Math.toDegrees(radians);
    }

    /**
     * Bound an angle (in degrees) to -180 to 180 degrees.
     * @param angle_degrees an input angle in degrees
     * @return the bounded angle
     */
    public static double boundHalfDegrees(double angle_degrees) {
        while (angle_degrees >= 180.0) angle_degrees -= 360.0;
        while (angle_degrees < -180.0) angle_degrees += 360.0;
        return angle_degrees;
    }

    /**
     * Generate a motion profile trajectory using the given waypoints and configuration.
     * @param waypoints     An array of waypoints (setpoints) for the trajectory path to intersect
     * @param config        The configuration of the trajectory, including acceleration, jerk
     *                      and other values such as time scale and fit method
     * @return              The generated trajectory (an array of segments)
     */
    public static Trajectory generate(Waypoint[] waypoints, Trajectory.Config config) throws Pathfinder.GenerationException {
        if (waypoints.length < 2) {
            //cant generate a trajectory
            return new Trajectory(0);
        }

        List<Spline> splinesL = new ArrayList<>();
        config.target_vels = new double[waypoints.length - 1];
        for (int i = 0; i < waypoints.length - 1; i++) {
            Spline s;
            if (config.fit == Trajectory.FitMethod.HERMITE_CUBIC)
                s = Fit.fitHermiteCubic(waypoints[i], waypoints[i + 1]);
            else
                s = Fit.fitHermiteQuintic(waypoints[i], waypoints[i + 1]);
            s.splineDistance(config.sample_count);
            splinesL.add(s);
            config.target_vels[i] = waypoints[i].velocity;
        }
        Spline[] splines = {new Spline()};
        splines = splinesL.toArray(splines);
        Trajectory traj = new Trajectory();
        traj.generateVelocity(config, splines);


        double spline_pos_init = 0;
        int Si = 0;
        double pos, percentage=0;
        Spline s;

        for (int i = 0; i < traj.segments.length; i++) {
            pos = traj.segments[i].position;

            boolean found = false;
            while (!found) {
                double pos_relative = pos - spline_pos_init;
                if (pos_relative <= splines[Si].arc_length) {
                    s = splines[Si];
                    percentage = s.splineProgressForDistance(pos_relative, config.sample_count);
                    Spline.Coord c = s.splineCoords(percentage);
                    traj.segments[i].x = c.x;
                    traj.segments[i].y = c.y;
                    traj.segments[i].heading = s.splineAngle(percentage);
                    found = true;
                } else if (Si < splines.length-1) {
                    spline_pos_init += splines[Si].arc_length;
                    Si++;
                } else {
                    s = splines[splines.length - 1];
                    Spline.Coord c = s.splineCoords(1);
                    traj.segments[i].x = c.x;
                    traj.segments[i].y = c.y;
                    traj.segments[i].heading = s.splineAngle(1);
                    found = true;
                }
            }
        }

        traj.addLookaheadValues();
        return traj;
    }

    /**
     * Write the Trajectory to a Binary (non human readable) file
     * @param file          The file to write to
     * @param trajectory    The trajectory to write
     */
    public static void writeToFile(File file, Trajectory trajectory) {
        PathfinderJNI.trajectorySerialize(trajectory.segments, file.getAbsolutePath());
    }

    /**
     * Read a Trajectory from a Binary (non human readable) file
     * @param file          The file to read from
     * @return              The trajectory that was read from file
     */
    public static Trajectory readFromFile(File file) {
        return new Trajectory(PathfinderJNI.trajectoryDeserialize(file.getAbsolutePath()));
    }

    /**
     * Write the Trajectory to a CSV File
     * @param file          The file to write to
     * @param trajectory    The trajectory to write
     */
    public static void writeToCSV(File file, Trajectory trajectory) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        StringBuilder builder = new StringBuilder();
        String columnNames = "dt,x,y,position,velocity,acceleration,jerk,heading,lookahead\n";
        builder.append(columnNames);

        //add lookahead before save to csv file
        trajectory.addLookaheadValues();
        for (Trajectory.Segment seg : trajectory.segments) {
            builder.append(String.format("%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                    seg.dt, seg.x, seg.y, seg.position, seg.velocity, seg.acceleration, seg.jerk, seg.heading, seg.lookaheadIndex));
        }

        pw.write(builder.toString());
        pw.close();
    }

    /**
     * Read a Trajectory from a CSV File
     * @param file          The file to read from
     * @return              The trajectory that was read from file
     */
    public static Trajectory readFromCSV(File file) {
        return new Trajectory(PathfinderJNI.trajectoryDeserializeCSV(file.getAbsolutePath()));
    }

    /**
     * Thrown when a Trajectory could not be generated for an unknown reason.
     */
    public static class GenerationException extends Exception {
        public GenerationException(String message) {
            super(message);
        }
    }

    /**
     * Thrown when a Trajectory is not useable.
     */
    public static class OutputException extends Exception {
        public OutputException(String message) {
            super(message);
        }
    }

}

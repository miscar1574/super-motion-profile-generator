package com.mammen.pathfinder;

public class Fit {

    //TODO: add comments

    public static Spline fitHermiteCubic(Waypoint a, Waypoint b) {
        Spline s = fitHermitePre(a, b);

        double a0_delta = Math.tan(a.angle - s.angle_offset);
        double a1_delta = Math.tan(b.angle - s.angle_offset);

        s.a = s.b = 0;
        s.c = (a0_delta + a1_delta) / Math.pow(s.knot_distance, 2);
        s.d = -(2 * a0_delta + a1_delta) / s.knot_distance;
        s.e = a0_delta;

        return s;
    }

    public static Spline fitHermiteQuintic(Waypoint a, Waypoint b) {
        Spline s = fitHermitePre(a, b);

        double a0_delta = Math.tan(a.angle - s.angle_offset);
        double a1_delta = Math.tan(b.angle - s.angle_offset);

        s.a = -(3 * (a0_delta + a1_delta)) / Math.pow(s.knot_distance, 4);
        s.b = (8 * a0_delta + 7 * a1_delta) / Math.pow(s.knot_distance, 3);
        s.c = -(6 * a0_delta + 4 * a1_delta) / Math.pow(s.knot_distance, 2);
        s.d = 0;
        s.e = a0_delta;

        return s;
    }

    private static Spline fitHermitePre(Waypoint a, Waypoint b) {
        Spline s = new Spline();

        s.x_offset = a.x;
        s.y_offset = a.y;

        double dx = b.x - a.x;
        double dy = b.y - a.y;
        s.knot_distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        s.angle_offset = Math.atan2(dy, dx);

        return s;
    }
}

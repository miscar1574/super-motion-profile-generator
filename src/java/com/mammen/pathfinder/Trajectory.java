package com.mammen.pathfinder;

import com.mammen.main.ProfileGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Trajectory {

    /**
     * The Trajectory Configuration outlines the rules to follow while generating the trajectory. This includes
     * the method used for 'fitting' the spline, the amount of samples to use, the time difference and maximum values
     * for the velocity, acceleration and jerk of the trajectory.
     */
    public static class Config {

        public static final int SAMPLES_FAST = 1000;
        public static final int SAMPLES_LOW = SAMPLES_FAST * 10;
        public static final int SAMPLES_HIGH = SAMPLES_LOW * 10;

        public FitMethod fit;
        public int sample_count;
        public double dt, max_acceleration, max_jerk;
        public double[] target_vels;
        public double startVelocty;
        public double endVelocity;

        /**
         * Create a Trajectory Configuration
         * @param fit                   The fit method to use
         * @param samples               How many samples to use to refine the path (higher = smoother, lower = faster)
         * @param dt                    The time delta between points (in seconds)
         * @param max_acceleration      The maximum acceleration to use (in meters per second per second)
         * @param max_jerk              The maximum jerk (acceleration per second) to use
         */
        public Config(FitMethod fit, int samples, double dt, double max_acceleration, double max_jerk, double startVelocty, double endVelocity) {
            this.fit = fit;
            this.sample_count = samples;
            this.dt = dt;
            this.max_acceleration = max_acceleration;
            this.max_jerk = max_jerk;
            this.startVelocty = startVelocty;
            this.endVelocity = endVelocity;
        }
    }

    /**
     * A Trajectory Segment is a particular point in a trajectory. The segment contains the xy position and the velocity,
     * acceleration, jerk and heading at this point
     */
    public static class Segment {
        public double dt, x, y, position, velocity, acceleration, jerk, heading, lookaheadIndex;

        Segment(double dt, double x, double y, double position, double velocity, double acceleration, double jerk, double heading) {
            this.dt = dt;
            this.x = x;
            this.y = y;
            this.position = position;
            this.velocity = velocity;
            this.acceleration = acceleration;
            this.jerk = jerk;
            this.heading = heading;
            this.lookaheadIndex = 0;
        }

        public Segment copy() {
            return new Segment(dt, x, y, position, velocity, acceleration, jerk, heading);
        }

        public boolean equals(Segment seg) {
            return  seg.dt == dt && seg.x == x && seg.y == y &&
                    seg.position == position && seg.velocity == velocity &&
                    seg.acceleration == acceleration && seg.jerk == jerk && seg.heading == heading;
        }

        public boolean fuzzyEquals(Segment seg) {
            return  ae(seg.dt, dt) && ae(seg.x, x) && ae(seg.y, y) && ae(seg.position, position) &&
                    ae(seg.velocity, velocity) && ae(seg.acceleration, acceleration) && ae(seg.jerk, jerk) &&
                    ae(seg.heading, heading);
        }

        public double calcDistance(Segment other){
            double dx = other.x - this.x;
            double dy = other.y - this.y;
            return Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));
        }

        private boolean ae(double one, double two) {
            return Math.abs(one - two) < 0.0001;        // Really small value
        }
    }

    /**
     * A Motion Info contains the necessary information to construct a section of the trajectory with a specific
     * type of motion (constant velocity, constant acceleration).
     * This information includes the acceleration, during that section, the length of time that it should be, and the
     * wanted final velocity.
     */
    private static class MotionInfo {
        double time, acceleration, targetVelocity;

        /**
         * Create a new Motion Info
         * @param time                  The length of time that this section should be.
         * @param acceleration          The acceleration during this section.
         * @param targetVelocity        The wanted final velocity of this section.
         */
        MotionInfo (double time, double acceleration, double targetVelocity) {
            this.time = time;
            this.acceleration = acceleration;
            this.targetVelocity = targetVelocity;
        }
    }

    /**
     * The Fit Method defines the function by which the trajectory path is generated. By default, the HERMITE_CUBIC method
     * is used.
     */
    public enum FitMethod {
        HERMITE_CUBIC, HERMITE_QUINTIC;
    }

    public Segment[] segments;

    public Trajectory() {
    }

    public Trajectory(Segment[] segments) {
        this.segments = segments;
    }

    public Trajectory(int length) {
        this.segments = new Segment[length];
    }

    public Segment get(int index) {
        return segments[index];
    }

    public int length() {
        return segments.length;
    }

    public Trajectory copy() {
        Trajectory toCopy = new Trajectory(length());
        for (int i = 0; i < length(); i++) {
            toCopy.segments[i] = get(i).copy();
        }
        return toCopy;
    }

    /**
     * Generate a motion profile trajectory using the given splines and configuration.
     * @param c             The configuration of the trajectory, including acceleration, jerk
     *                      and other values such as time scale and fit method
     * @param splines       An array of the different splines constructing the path.
     *
     * @return              The generated trajectory (an array of segments), without the real-world positions.
     *
     */
    public Segment[] generateVelocity(Config c, Spline[] splines) throws Pathfinder.GenerationException {
        //FIXME: if start or end velocity are higher then the nera waypoint the trajectory is bad
        final double ENDVELOCITY = c.endVelocity;    //the final velocity of the trajectory - keep it 0 until fully checked
        final double STARTVELOCITY = c.startVelocty; //the starting velocity of the trajectory - keep it 0 until fully checked

        double lastVelocity = STARTVELOCITY;         //the velocity in the spline before
        double currentVelocity;                      //the target velocity in the current spline
        double nextVelocity;                         //the velocity of the next spline

        double length;                               //the current spline linear length

        double accTime, decTime;                     //time need to accelerate and decelerate
        double accDist, decDist;                     //distance need to accelerate and decelerate
        double mvt;                                  //time at current spline velocity
        double smv;                                  //distance at current spline velocity

        List<MotionInfo> motionInfos = new ArrayList<>();

        for (int i = 0; i < splines.length; i++) {
            length = splines[i].arc_length;
            currentVelocity = c.target_vels[i];
            nextVelocity = i + 1 < splines.length ? c.target_vels[i + 1] : ENDVELOCITY; // force the last spline to end at 0 speed

            accTime = decTime = 0;

            if (currentVelocity > lastVelocity) //calc time need accelerate from the spline before
                accTime = (currentVelocity - lastVelocity) / c.max_acceleration;

            if (currentVelocity > nextVelocity) //calc time need to decelerate for the next spline
                decTime = (currentVelocity - nextVelocity) / c.max_acceleration;

            accDist = lastVelocity * accTime + 0.5 * c.max_acceleration * Math.pow(accTime, 2);//distance to accelerate
            decDist = nextVelocity * decTime + 0.5 * c.max_acceleration * Math.pow(decTime, 2);//distance to decelerate

            smv = length - (accDist + decDist);
            if(smv < 0) {
                /* the robot cant reach the velocity the user asked for at this waypoint
                 * so it will draw the rest of the traj*/
                throw new Pathfinder.GenerationException("the trajectory cant be generated since a velocity issue found at waypoint "+(i+1));
            }

            mvt = smv / currentVelocity;
            if(accTime>0) motionInfos.add(new MotionInfo(accTime, c.max_acceleration, currentVelocity));
            if(mvt>0) motionInfos.add(new MotionInfo(mvt, 0, currentVelocity));
            if(decTime>0) motionInfos.add(new MotionInfo(decTime, -c.max_acceleration, nextVelocity));
            lastVelocity = currentVelocity;
        }

        /**
        * estimate how much points there are in the profile
        * since in some cases the generated profile contains a huge number of points that cant be in
        * a node without returning out of memory error
        *
        * the number can be change in the settings menu
        */
        int estimatedNumberOfPoints = 0;
        for(int index = 0; index < motionInfos.size(); index++){
            estimatedNumberOfPoints += motionInfos.get(index).time/c.dt;
        }
        if(estimatedNumberOfPoints > ProfileGenerator.maxNumberOfPoints) {
            /* there are to much points
            the generate process will be stopped and the user will be noted */
            throw new Pathfinder.GenerationException(String.format("there are to much points in the profile(%s) the maximum is %s" +
                            " but it can be changed in the settings menu", estimatedNumberOfPoints, ProfileGenerator.maxNumberOfPoints));
        }


        /** Find the position, velocity and acceleration of each segment */

        double position = 0;                         //the current segment linear position
        double dstPos = 0;                           //the last motionInfo linear position

        double velocity = STARTVELOCITY;             //the starting velocity of the trajectory
        double acceleration;                         //the acceleration in the current motionInfo

        double dt;                                   //the dt of the current segment
        final double minDt = c.dt/4;                 //the min dt for a segment - if lower the segment will not be added
        double numberOfPoints;                       //the estimated number of points in the current motionInfo
        double lastTarget, nextTarget;               //the target velocity in the after and before motionInfo
        lastVelocity = 0;                            //the velocity the last motionInfo ended at

        List<Segment> segments = new ArrayList<>();


        if(motionInfos.size() > 0)
            segments.add(new Segment(c.dt,0,0,0,STARTVELOCITY, motionInfos.get(0).acceleration,0,0));
        for (int t = 0; t < motionInfos.size(); t++) { //run on every motionInfo added in the loop before
            numberOfPoints = (motionInfos.get(t).time / c.dt) - 1;
            for (int i = 0; i < numberOfPoints; i++) {//the last one is added later
                /**
                 * since the number of points using a constant dt is not a pure integer the last point must
                 * be with different dt otherwise the position might go over this section target position which is a cause
                 * for a lot of problems
                 *
                 * if this is the last point of this section(actually one before the last which is added outside of the for loop)
                 * the dt will be different then the constant dt
                 */
                dt =c.dt * ((int)(numberOfPoints) == i ? (numberOfPoints - (int)(numberOfPoints)) : 1);
                acceleration = motionInfos.get(t).acceleration;
                velocity += acceleration * dt;
                /* the next section is for keeping the velocity in the range of the target velocity and not higher */
                if (acceleration > 0)
                    velocity = velocity > motionInfos.get(t).targetVelocity ? motionInfos.get(t).targetVelocity : velocity;
                else if (acceleration < 0)
                    velocity = velocity < motionInfos.get(t).targetVelocity ? motionInfos.get(t).targetVelocity : velocity;

                /* update linear position and add segment*/
                position += lastVelocity * dt + 0.5 * acceleration * Math.pow(dt, 2);
                lastVelocity = velocity;
                if(dt > minDt) segments.add(new Segment(dt, 0, 0, position, velocity, acceleration, 0, 0));

            }
            if (motionInfos.get(t).time > 0) {//the time can be 0 if the robot doesn't need to accelerate or decelerate

                /* if we are in the first section the last target velocity is set to STARTVELOCITY */
                lastTarget = t - 1 >= 0 ? motionInfos.get(t-1).targetVelocity : STARTVELOCITY;
                dstPos += lastTarget * motionInfos.get(t).time + 0.5 * motionInfos.get(t).acceleration * Math.pow(motionInfos.get(t).time, 2);
                position = dstPos;

                /* if the robot is at the last section end at ENDVELOCITY velocity */
                nextTarget = t + 1 < motionInfos.size() ? motionInfos.get(t + 1).targetVelocity : ENDVELOCITY;

                velocity = motionInfos.get(t).acceleration >= 0 ? motionInfos.get(t).targetVelocity : nextTarget;//
                lastVelocity = velocity;
                segments.add(new Segment(c.dt, 0, 0, position, velocity, 0, 0, 0));
            }

        }

        /*
         * in case "segments" is empty - not even part of the path can be done, we want to return "this.segments" not empty or full with nulls
         * so we should insert "temp" - the default array, but the toArray() function remove every value in "temp" so
         * it will add the default value to this.segments after the toArray ran nad not to "temp" before the function runs
         */
        Segment[] temp = new Segment[1];
        this.segments = segments.toArray(temp);
        if(this.segments.length == 1) this.segments[0] = new Segment(c.dt,0,0,0,0,0,0,0);
        return this.segments;
    }

    /**
     * add lookahead index value for every segment on the trajectory
     *
     * the solution is not the most effective but its more readable than others
     */
    public void addLookaheadValues(){
        double lastDist, currentDist = 0;
        final double lookaheadDistance = ProfileGenerator.lookaheadDist;
        int lookaheadIndex;
        for(int index = 0; index < this.segments.length - 1; index++){
            currentDist = Double.MAX_VALUE;
            lookaheadIndex = index + 1;
            do {
                lastDist = currentDist;
                currentDist = Math.abs(this.segments[lookaheadIndex].calcDistance(this.segments[index]) - lookaheadDistance);
                lookaheadIndex = lookaheadIndex < this.segments.length - 1 ? lookaheadIndex + 1: lookaheadIndex;
            }
            while (lastDist > currentDist);

            this.segments[index].lookaheadIndex = lookaheadIndex-1;
        }

        this.segments[this.segments.length-1].lookaheadIndex = this.segments.length-1;
    }

    /**
     * check if there are points that go backward in the trajectory
     */
    public void findBadPoints() throws Pathfinder.OutputException {
        //bad points occur if the linear position is not in constant growing
        for (int i = 0; i < this.segments.length; i++) {
            if (segmentIsBadPoint(i))
                throw new Pathfinder.OutputException("there is a at list one bad point in your trajectory\n " +
                        "make sure to check it before uploading to the robot");
        }
    }

    public boolean segmentIsBadPoint(int index){
        if(index == 0 || index == segments.length-1) return false;
        return segments[index].position > segments[index+1].position;
    }

}

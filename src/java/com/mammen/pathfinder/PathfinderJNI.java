package com.mammen.pathfinder;

import com.mammen.pathfinder.modifiers.SwerveModifier;

public class PathfinderJNI {

    static boolean libLoaded = false;

    static {
        if (!libLoaded) {
            try {
                System.loadLibrary("pathfinderjava");
            } catch (Exception e) {
                e.printStackTrace();
            }
            libLoaded = true;
        }
    }

    public static Trajectory[] modifyTrajectorySwerve(Trajectory traj, double wheelbase_width, double wheelbase_depth, SwerveModifier.Mode mode) {
        Trajectory.Segment[][] mod = modifyTrajectorySwerve(traj.segments, wheelbase_width, wheelbase_depth, mode);
        return new Trajectory[] { new Trajectory(mod[0]), new Trajectory(mod[1]), new Trajectory(mod[2]), new Trajectory(mod[3]) };
    }
    public static native Trajectory.Segment[][] modifyTrajectorySwerve(Trajectory.Segment[] source, double wheelbase_width, double wheelbase_depth, SwerveModifier.Mode mode);

    public static native void trajectorySerialize(Trajectory.Segment[] source, String filename);
    public static native Trajectory.Segment[] trajectoryDeserialize(String filename);

    public static native void trajectorySerializeCSV(Trajectory.Segment[] source, String filename);
    public static native Trajectory.Segment[] trajectoryDeserializeCSV(String filename);
}

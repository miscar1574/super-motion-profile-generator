package com.mammen.main;

import com.mammen.ssh.Path;
import com.mammen.util.Paths;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application
{
	public static Scene mainScene;
	@Override
	public void start(Stage primaryStage)
	{
		try
		{
			Pane root = FXMLLoader.load(getClass().getResource("/com/mammen/ui/javafx/MainUI.fxml") );
	        root.autosize();

	        /*initialize to dark mode*/
			mainScene = new Scene(root);
			//TODO: make start up theme editable on settings
			mainScene.getStylesheets().add(getClass().getResource(Paths.darkMode).toExternalForm());

			primaryStage.setScene(mainScene);
			primaryStage.sizeToScene();
			primaryStage.setTitle("Miscar's Motion Profile Generator");

			//TODO: max width that depends on the image and field size
			primaryStage.setMaxWidth(1650);
			primaryStage.setMaxHeight(790);

	        primaryStage.setResizable(true);
			primaryStage.show();
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{	
		launch(args);
	}
}

package com.mammen.ui.javafx.dialog;

import com.mammen.ui.javafx.MainUIController;
import com.mammen.ui.javafx.PropWrapper;
import com.mammen.util.Paths;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.Properties;

public class SettingsDialogController {
    @FXML
    private Pane root;

    @FXML
    private TextField txtOverlayDir;

    @FXML
    private TextField txtTargetIP;

    @FXML
    private TextField txtMaxPoints;

    @FXML
    private TextField txtTargetPath;

    @FXML
    private TextField txtFieldWidth;

    @FXML
    private TextField txtFieldHeight;

    @FXML
    private Button btnChooseOverlay;

    @FXML
    private ChoiceBox<String> choSourceDisplay;
    
    @FXML
    private ChoiceBox<String> choCSVType;

    @FXML
    private CheckBox chkAddWaypointOnClick;

    @FXML
    private Label titleExUp, titleDisplay;

    private Properties properties;

    @FXML
    private void initialize() {
        properties = PropWrapper.getProperties();
        txtOverlayDir.setText(properties.getProperty("ui.overlayDir", ""));
        choSourceDisplay.setItems(FXCollections.observableArrayList("None", "Waypoints only", "Waypoints + Source"));
        choSourceDisplay.getSelectionModel().select(Integer.parseInt(properties.getProperty("ui.sourceDisplay", "2")));
        choCSVType.setItems(FXCollections.observableArrayList("Jaci", "Talon SRX"));
        choCSVType.getSelectionModel().select(Integer.parseInt(properties.getProperty("ui.csvType", "0")));
        chkAddWaypointOnClick.setSelected(Boolean.parseBoolean(properties.getProperty("ui.addWaypointOnClick", "false")));
        txtTargetIP.setText(properties.getProperty("ui.targetIP", "10.15.74.2"));
        txtTargetPath.setText(properties.getProperty("ui.targetPath", "/lvuser/paths"));
        txtMaxPoints.setText(properties.getProperty("ui.maxPoints", "1000"));
        txtFieldWidth.setText(properties.getProperty("ui.fieldWidth", "15.98"));
        txtFieldHeight.setText(properties.getProperty("ui.fieldHeight", "8.21"));

        //TODO: find a better solution, at list read the color from the css
        if(MainUIController.ThemeIsOn(Paths.darkMode)) {
            final String darkBackground = "#2e3032"; //the default background in dark mode
            titleDisplay.setStyle("-fx-background-color: " + darkBackground);
            titleExUp.setStyle("-fx-background-color: " + darkBackground);
        }

    }

    @FXML
    private void showChooseOverlayDialog() {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        fileChooser.setTitle("Find Position Map Overlay");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter(
                        "Image Files",
                        "*.jpg",
                        "*.jpeg",
                        "*.png"
                )
        );

        File result = fileChooser.showOpenDialog(root.getScene().getWindow());

        if (result != null && result.exists() && !result.isDirectory()) {
            txtOverlayDir.setText(result.getAbsolutePath());
        }
    }

    //the writing to the properties file are made in the MainUIController
    //if you wish to add a property than also add it to the PropWrapper
}

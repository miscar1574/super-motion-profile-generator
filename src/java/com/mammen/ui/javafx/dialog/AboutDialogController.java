package com.mammen.ui.javafx.dialog;

import com.mammen.ui.javafx.ResourceLoader;
import com.mammen.util.Paths;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Controller for the About dialog
 * Mainly just to initialize the links and version number
 */
public class AboutDialogController {
    @FXML
    private Label lblVersion;

    @FXML
    private Hyperlink
            hlLukeGithub,
            hlBlakeGithub,
            hlJaciGithub,
            hlVannakaGithub,
            hlmiscar,
            hlMITLicense;

    @FXML
    private void initialize() {

        lblVersion.setText("v" + getVersion());

        hlLukeGithub.setOnAction((ActionEvent e) -> openLink("https://github.com/vannaka"));
        hlBlakeGithub.setOnAction((ActionEvent e) -> openLink("https://github.com/blake1029384756"));
        hlJaciGithub.setOnAction((ActionEvent e) -> openLink("https://github.com/JacisNonsense"));
        hlMITLicense.setOnAction((ActionEvent e) -> openLink("https://opensource.org/licenses/MIT"));
        hlmiscar.setOnAction((ActionEvent e) -> openLink("https://www.miscar1574.org"));
    }

    private void openLink(String url) {
        try {
            Desktop.getDesktop().browse(new URI(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getVersion(){
        Manifest manifest = ResourceLoader.getManifest();

        if (manifest != null) {
            Attributes mfAttr = manifest.getMainAttributes();
            String mfVersion = mfAttr.getValue("Version");
            if (mfVersion != null)
                return mfVersion;
        }
        try{
            String version = "--.--.--";
            String temp;
            File file=new File(Paths.changeLog);    //creates a new file instance
            FileReader fr=new FileReader(file);          //reads the file
            BufferedReader br=new BufferedReader(fr);    //creates a buffering character input stream
            String line;
            while((line=br.readLine())!=null)
            {
                if(line.indexOf('[') != -1 && line.indexOf(']') != -1) {
                    temp = line.substring(line.indexOf('[') + 1, line.indexOf(']'));
                    if(temp.indexOf(".") != -1){ //every version include a . in it
                        version = temp;
                        break;
                    }
                }
            }
            fr.close();
            return version;
        }
        catch (Exception e){}
        return "";

    }
}

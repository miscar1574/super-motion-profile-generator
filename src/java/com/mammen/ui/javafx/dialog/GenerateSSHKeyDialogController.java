package com.mammen.ui.javafx.dialog;

import com.mammen.util.Paths;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class GenerateSSHKeyDialogController {

    private ObservableList<step> steps;

    @FXML
    private ListView stepList;

    @FXML
    private void initialize(){
        steps = FXCollections.observableArrayList();

        stepList.setItems(steps);
        //TODO: write instructions and steps in the .txt file
        //TODO: make dialog resizeable
        //Accordion
        loadSteps(steps);
        stepList.setCellFactory(lv -> {
            ListCell<step> cell = new ListCell<>() {
                @Override
                protected void updateItem(step step, boolean empty) {
                    super.updateItem(step, empty);
                    if(!isEmpty()){
                        setText(getItem().toString());

                        if(getItem() instanceof title) setStyle(
                                "-fx-underline: true;"+
                                "-fx-font-size: 14");

                        else if(getItem() instanceof comment) setStyle(
                                "-fx-text-fill: #676767");

                        else if(getItem() instanceof bigTitle) setStyle(
                                "-fx-font-size: 16;"+
                                "-fx-font-weight: bold");

                        else setStyle("");
                    }
                }
            };
            return cell;
        });
    }

    private void loadSteps(ObservableList<step> list){
        try {
            File file=new File(Paths.sshKeyGenerationInstructions);
            FileReader fr=new FileReader(file);
            BufferedReader br=new BufferedReader(fr);
            String line;
            while((line=br.readLine())!=null) {
                if(line.length() == 0) list.add(new step(""));

                if     (line.charAt(0) == '#') list.add(new title(line.substring(1)));      //small title
                else if(line.charAt(0) == '$') list.add(new bigTitle(line.substring(1)));   //big title
                else if(line.charAt(0) == '/') list.add(new comment(line.substring(1)));    //comment
                else if(line.charAt(0) == '-') list.add(new step(""));                 //empty line
                else list.add(new step(line));                                              //regular line
            }
        }
        catch (Exception e){ System.out.println(e); }
    }


    private static class step{
        private String text;

        public step(String text){ this.text = text; }

        @Override
        public String toString() { return text; }
    }

    private static class title extends step{
        public title(String text){
            super(text);
        }
    }

    private static class bigTitle extends step{
        public bigTitle(String text){
            super(text);
        }
    }

    private static class comment extends step{
        public comment(String text){
            super(text);
        }
    }
}

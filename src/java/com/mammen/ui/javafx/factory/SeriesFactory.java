package com.mammen.ui.javafx.factory;


import com.mammen.pathfinder.Trajectory;
import com.mammen.pathfinder.Waypoint;
import javafx.scene.chart.XYChart;
import org.w3c.dom.ls.LSOutput;

public class SeriesFactory
{
	private SeriesFactory() { }

    public static XYChart.Series<Double, Double> buildPositionSeries(Trajectory t) {
        XYChart.Series<Double, Double> series = new XYChart.Series<>();

        for (int i = 0; i < t.segments.length; i++) {
            XYChart.Data<Double, Double> data = new XYChart.Data<>();

            data.setXValue(t.get(i).x);
            data.setYValue(t.get(i).y);

            series.getData().add(data);
        }
        return series;
    }

    public static XYChart.Series<Double, Double> buildVelocitySeries(Trajectory t) {
        XYChart.Series<Double, Double> series = new XYChart.Series<>();

        double xValue = -t.get(0).dt;
        for (int i = 0; i < t.segments.length; i++) {
            XYChart.Data<Double, Double> data = new XYChart.Data<>();

            xValue += t.get(i).dt;
            data.setXValue(xValue);
            data.setYValue(t.get(i).velocity);
            series.getData().add(data);
        }
        return series;
    }

    public static XYChart.Series<Double, Double> buildWaypointsSeries(Waypoint[] waypoints) {
        XYChart.Series<Double, Double> series = new XYChart.Series<>();

        for (Waypoint w : waypoints) {
            XYChart.Data<Double, Double> data = new XYChart.Data<>();

            data.setXValue(w.x);
            data.setYValue(w.y);
            series.getData().add(data);

        }
        return series;
    }

    public static XYChart.Series<Double, Double> buildWaypointsVelSeries(Waypoint[] waypoints, Trajectory t) {
        XYChart.Series<Double, Double> series = new XYChart.Series<>();
        if(t == null || waypoints == null || t.length() == 0 || waypoints.length == 0){
            return series;
        }

        double time = -t.get(0).dt; int waypointIndex = 0;
        for (int i = 0; i < t.segments.length ; i++) {//&& waypointIndex < waypoints.length
            time += t.get(i).dt;
            if(Math.abs(waypoints[waypointIndex].x - t.get(i).x)<0.000002 &&
                    Math.abs(waypoints[waypointIndex].y - t.get(i).y) < 0.000002) {
                XYChart.Data<Double, Double> data = new XYChart.Data<>();
                data.setXValue(time);
                data.setYValue(t.get(i).velocity);
                //data.setYValue(waypoints[waypointIndex].velocity);
                series.getData().add(data);
                waypointIndex += waypointIndex<waypoints.length-1? 1:0;
            }
        }
        return series;
	}

}

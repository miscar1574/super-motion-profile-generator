package com.mammen.ui.javafx;

import com.mammen.main.Main;
import com.mammen.main.ProfileGenerator;
import com.mammen.pathfinder.Pathfinder;
import com.mammen.pathfinder.Trajectory;
import com.mammen.pathfinder.Upload;
import com.mammen.pathfinder.Waypoint;
import com.mammen.ui.javafx.factory.AlertFactory;
import com.mammen.ui.javafx.factory.DialogFactory;
import com.mammen.ui.javafx.factory.SeriesFactory;
import com.mammen.util.Mathf;
import com.mammen.util.OSValidator;
import com.mammen.util.Paths;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ObservableValueBase;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javafx.util.converter.DoubleStringConverter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

import static com.mammen.util.Mathf.meterToFeet;
import static com.mammen.util.Mathf.meterToInch;
import static com.mammen.util.Mathf.round;
//TODO: organise mainUI.fxml - zaks
public class MainUIController {

    private ProfileGenerator backend;

    @FXML
    private Pane root;

    @FXML
    private TextField txtTimeStep,
            txtAcceleration,
            txtWheelBaseW,
            txtWheelBaseD,
            txtLookaheadDist,
            txtEndVelocity,
            txtStartVelocity;

    @FXML
    private Label lblWheelBaseD;

    @FXML
    private TableView<Waypoint> tblWaypoints;

    @FXML
    private TableView<Trajectory.Segment> tblTrajectory;

    @FXML
    private LineChart<Double, Double> chtPosition,
            chtVelocity;

    @FXML
    private NumberAxis axisPosX,
            axisPosY,
            axisTime,
            axisVel;

    @FXML
    private TableColumn<Waypoint, Double>
            colWaypointX,
            colWaypointY,
            colWaypointAngle,
            colWaypointVel;

    @FXML
    private TableColumn<Trajectory.Segment, Double>
            colTrajX,
            colTrajY,
            colTrajDt,
            colTrajHeading,
            colTrajHeadingDeg,
            colTrajPosition,
            colTrajAc,
            colTrajLookahead,
            colTrajVelocity;

    @FXML
    private MenuItem
            mnuOpen,
            mnuFileNew,
            mnuFileSave,
            mnuFileSaveAs,
            mnuFileExport,
            mnuFileExit,
            mnuTheme,
            mnuQuickUpload,
            mnuHelpAbout;

    @FXML
    private ChoiceBox<String>
            choDriveBase,
            choFitMethod,
            choUnits;

    @FXML
    private Button
             btnAddPoint,
            btnClearPoints,
            btnDelete;

    private ObservableList<Waypoint> waypointsList;

    private ObservableList<Trajectory.Segment> segments;

    private Properties properties;

    private Upload upload;

    private File workingDirectory;

    private static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat("application/x-java-serialized-object");

    @FXML
    public void initialize() {
        backend = new ProfileGenerator();
        properties = PropWrapper.getProperties();
        workingDirectory = new File(System.getProperty("user.dir"));

        upload = new Upload();
        disablequickupload();
        /**update from settings file*/
        updateTrajectoryGenerationSettings();
        updateUploadSettings();

        /** fields settings*/
        {
            btnDelete.setDisable(true);

            choDriveBase.getItems().addAll("Tank", "Swerve");
            choDriveBase.setValue(choDriveBase.getItems().get(0));
            choDriveBase.getSelectionModel().selectedItemProperty().addListener(this::updateDriveBase);

            choFitMethod.getItems().addAll("Cubic", "Quintic");
            choFitMethod.setValue(choFitMethod.getItems().get(0));
            choFitMethod.getSelectionModel().selectedItemProperty().addListener(this::updateFitMethod);

            choUnits.getItems().addAll("Meters", "Inches", "Feet");
            choUnits.setValue(choUnits.getItems().get(0));
            choUnits.getSelectionModel().selectedItemProperty().addListener(this::updateUnits);
            updateChartAxis();

            txtTimeStep.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
            txtAcceleration.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
            txtWheelBaseW.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
            txtWheelBaseD.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
            txtLookaheadDist.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
            txtEndVelocity.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
            txtStartVelocity.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));

            //FIXME: since both the focus property of each text field and the validateFieldEdit function call the update trajectories the warning bug occur but we still want both of them
            txtLookaheadDist.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) // On unfocus
                {
                    String val = txtLookaheadDist.getText().trim();
                    double d = 0;

                    if (val.isEmpty()) {
                        val = "0.4";
                        txtLookaheadDist.setText(val);
                    } else {
                        d = Double.parseDouble(val);
                        if (d != 0) {
                            txtLookaheadDist.setText("" + Math.abs(d));
                            generateTrajectories();
                        }
                    }
                }
            });
            txtTimeStep.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) // On unfocus
                {
                    String val = txtTimeStep.getText().trim();
                    double d = 0;

                    if (val.isEmpty()) {
                        val = "0.02";
                        txtTimeStep.setText(val);
                    } else {
                        d = Double.parseDouble(val);
                        if (d != 0) {
                            txtTimeStep.setText("" + Math.abs(d));
                            //generateTrajectories();
                        }
                    }
                }
            });
            txtAcceleration.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) // On unfocus
                {
                    String val = txtAcceleration.getText().trim();
                    double d = 0;

                    if (val.isEmpty()) {
                        val = "3.0";
                        txtAcceleration.setText(val);
                    } else {
                        d = Double.parseDouble(val);
                        if (d != 0) {
                            txtAcceleration.setText("" + Math.abs(d));
                            //generateTrajectories();
                        }
                    }
                }
            });
            txtWheelBaseW.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) // On unfocus
                {
                    String val = txtWheelBaseW.getText().trim();
                    double d = 0;

                    if (val.isEmpty()) {
                        val = "1.464";
                        txtWheelBaseW.setText(val);
                    } else {
                        d = Double.parseDouble(val);
                        if (d != 0) {
                            txtWheelBaseW.setText("" + Math.abs(d));
                            generateTrajectories();
                        }
                    }
                }
            });
            txtWheelBaseD.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) // On unfocus
                {
                    String val = txtWheelBaseD.getText().trim();
                    double d = 0;

                    if (val.isEmpty()) {
                        val = "1.464";
                        txtWheelBaseD.setText(val);
                    } else {
                        d = Double.parseDouble(val);
                        if (d != 0) {
                            txtWheelBaseD.setText("" + Math.abs(d));
                            generateTrajectories();
                        }
                    }
                }
            });
            txtEndVelocity.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) // On unfocus
                {
                    String val = txtEndVelocity.getText().trim();
                    double d = 0;

                    if (val.isEmpty()) {
                        System.out.println("i was enmty");
                        val = "0";
                        txtEndVelocity.setText(val);
                    } else {
                        d = Double.parseDouble(val);
                        if (d >= 0) {
                            txtEndVelocity.setText("" + Math.abs(d));
                            generateTrajectories();
                        }
                    }
                }
            });
            txtStartVelocity.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) // On unfocus
                {
                    String val = txtStartVelocity.getText().trim();
                    double d = 0;

                    if (val.isEmpty()) {
                        val = "0";
                        txtStartVelocity.setText(val);
                    } else {
                        d = Double.parseDouble(val);
                        if (d >= 0) {
                            txtStartVelocity.setText("" + Math.abs(d));
                            generateTrajectories();
                        }
                    }
                }
            });
        }

        /** Waypoints table settings*/
        {
            Callback<TableColumn<Waypoint, Double>, TableCell<Waypoint, Double>> doubleCallback =
                    (TableColumn<Waypoint, Double> param) -> {
                        TextFieldTableCell<Waypoint, Double> cell = new TextFieldTableCell<>();

                        cell.setConverter(new DoubleStringConverter());

                        return cell;
                    };

            EventHandler<TableColumn.CellEditEvent<Waypoint, Double>> editHandler =
                    (TableColumn.CellEditEvent<Waypoint, Double> t) -> {
                        Waypoint curWaypoint = t.getRowValue();

                        if (t.getTableColumn() == colWaypointVel)
                            curWaypoint.velocity = t.getNewValue();
                        else if (t.getTableColumn() == colWaypointAngle)
                            curWaypoint.angle = Pathfinder.d2r(t.getNewValue());
                        else if (t.getTableColumn() == colWaypointY)
                            curWaypoint.y = t.getNewValue();
                        else
                            curWaypoint.x = t.getNewValue();

                        generateTrajectories();
                    };

            colWaypointX.setCellFactory(doubleCallback);
            colWaypointY.setCellFactory(doubleCallback);
            colWaypointAngle.setCellFactory(doubleCallback);
            colWaypointVel.setCellFactory(doubleCallback);

            colWaypointX.setOnEditCommit(editHandler);
            colWaypointY.setOnEditCommit(editHandler);
            colWaypointAngle.setOnEditCommit(editHandler);
            colWaypointVel.setOnEditCommit(editHandler);

            colWaypointX.setCellValueFactory((TableColumn.CellDataFeatures<Waypoint, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return d.getValue().x;
                }
            });

            colWaypointY.setCellValueFactory((TableColumn.CellDataFeatures<Waypoint, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return d.getValue().y;
                }
            });

            colWaypointAngle.setCellValueFactory((TableColumn.CellDataFeatures<Waypoint, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(Pathfinder.r2d(d.getValue().angle), 2);
                }
            });

            colWaypointVel.setCellValueFactory((TableColumn.CellDataFeatures<Waypoint, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return d.getValue().velocity;
                }
            });

            waypointsList = FXCollections.observableList(backend.getWaypointsList());
            waypointsList.addListener((ListChangeListener<Waypoint>) c ->
            {
                btnClearPoints.setDisable(waypointsList.size() == 0);
                if (!generateTrajectories())
                    waypointsList.remove(waypointsList.size());
            });

            tblWaypoints.setItems(waypointsList);
            tblWaypoints.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            tblWaypoints.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) ->
                    btnDelete.setDisable(tblWaypoints.getSelectionModel().getSelectedIndices().get(0) == -1)
            );
            /*drag & drop */
            tblWaypoints.setRowFactory(tv -> {
                TableRow<Waypoint> row = new TableRow<>();

                row.setOnDragDetected(event -> {
                    if (!row.isEmpty()) {
                        Integer index = row.getIndex();
                        Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                        db.setDragView(row.snapshot(null, null));
                        ClipboardContent cc = new ClipboardContent();
                        cc.put(SERIALIZED_MIME_TYPE, index);
                        db.setContent(cc);
                        event.consume();
                    }
                });

                row.setOnDragOver(event -> {
                    Dragboard db = event.getDragboard();
                    if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                        if (row.getIndex() != ((Integer) db.getContent(SERIALIZED_MIME_TYPE)).intValue()) {
                            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                            event.consume();
                        }
                    }
                });

                row.setOnDragDropped(event -> {
                    Dragboard db = event.getDragboard();
                    if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                        int draggedIndex = (Integer) db.getContent(SERIALIZED_MIME_TYPE);
                        Waypoint draggedRow = tblWaypoints.getItems().remove(draggedIndex);
                        int dropIndex;

                        if (row.isEmpty()) {
                            dropIndex = tblWaypoints.getItems().size();
                        } else {
                            dropIndex = row.getIndex();
                        }
                        tblWaypoints.getItems().add(dropIndex, draggedRow);

                        event.setDropCompleted(true);
                        tblWaypoints.getSelectionModel().select(dropIndex);
                        event.consume();
                    }
                });

                return row;
            });
            colTrajAc.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return d.getValue().acceleration;
                }
            });
        }

        /** Table view tab settings*/
        {
            final int NUMofDIGITSAFTERPOINT = 4;
            colTrajDt.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().dt,NUMofDIGITSAFTERPOINT);
                }
            });
            colTrajHeading.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().heading,NUMofDIGITSAFTERPOINT);
                }
            });
            colTrajHeadingDeg.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().heading * (180 / Math.PI),NUMofDIGITSAFTERPOINT);
                }
            });
            colTrajPosition.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().position,NUMofDIGITSAFTERPOINT);
                }
            });
            colTrajX.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().x,NUMofDIGITSAFTERPOINT);
                }
            });
            colTrajY.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().y,NUMofDIGITSAFTERPOINT);
                }
            });
            colTrajLookahead.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().lookaheadIndex,NUMofDIGITSAFTERPOINT);
                }
            });
            colTrajVelocity.setCellValueFactory((TableColumn.CellDataFeatures<Trajectory.Segment, Double> d) -> new ObservableValueBase<Double>() {
                @Override
                public Double getValue() {
                    return round(d.getValue().velocity,NUMofDIGITSAFTERPOINT);
                }
            });

            segments = FXCollections.observableArrayList();
            tblTrajectory.setItems(segments);

            tblTrajectory.setRowFactory(tv -> {
                TableRow<Trajectory.Segment> row = new TableRow<>() {
                    @Override
                    protected void updateItem(Trajectory.Segment entityWrapper, boolean empty) {
                        super.updateItem(entityWrapper, empty);
                        if (!isEmpty()) {
                            if (getItem().dt != backend.getTimeStep()) setStyle("-fx-background-color: orange");
                            else if (backend.getSourceTrajectory().segmentIsBadPoint(getIndex()))
                                setStyle("-fx-background-color: #750000");
                            else if (backend.segmentIsWayPoint(getIndex()))
                                setStyle("-fx-background-color: rgba(10,104,6,0.77)");
                            else setStyle("");
                            //TODO add legend in gui - zaks
                        }
                        else{setStyle("");}
                    }
                };
                return row;

            });
        }

        updateOverlayImg();
        updateFrontend();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            properties.setProperty("file.workingDir", workingDirectory.getAbsolutePath());
            try {
                PropWrapper.storeProperties();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

    /** Dialogs*/
    @FXML
    private void showSettingsDialog() {
        Dialog<Boolean> settingsDialog = DialogFactory.createSettingsDialog();
        Optional<Boolean> result = null;

        // Wait for the result
        result = settingsDialog.showAndWait();

        result.ifPresent((Boolean b) -> {
            if (b) {
                try {
                    DialogPane pane = settingsDialog.getDialogPane();

                    String overlayDir = ((TextField) pane.lookup("#txtOverlayDir")).getText().trim();
                    String targetIP = ((TextField) pane.lookup("#txtTargetIP")).getText().trim();
                    String targetPath = ((TextField) pane.lookup("#txtTargetPath")).getText().trim();
                    String maxPoints = ((TextField) pane.lookup("#txtMaxPoints")).getText().trim();
                    String fieldHeight = ((TextField) pane.lookup("#txtFieldHeight")).getText().trim();
                    String fieldWidth = ((TextField) pane.lookup("#txtFieldWidth")).getText().trim();


                    int sourceDisplay = ((ChoiceBox<String>) pane.lookup("#choSourceDisplay"))
                            .getSelectionModel()
                            .getSelectedIndex();

                    int csvType = ((ChoiceBox<String>) pane.lookup("#choCSVType"))
                            .getSelectionModel()
                            .getSelectedIndex();

                    boolean addWaypointOnClick = ((CheckBox) pane.lookup("#chkAddWaypointOnClick")).isSelected();

                    properties.setProperty("ui.overlayDir", overlayDir);
                    properties.setProperty("ui.sourceDisplay", "" + sourceDisplay);
                    properties.setProperty("ui.addWaypointOnClick", "" + addWaypointOnClick);
                    properties.setProperty("ui.csvType", "" + csvType);
                    properties.setProperty("ui.targetIP", targetIP);
                    properties.setProperty("ui.targetPath", targetPath);
                    properties.setProperty("ui.maxPoints", maxPoints);
                    properties.setProperty("ui.fieldWidth", fieldWidth);
                    properties.setProperty("ui.fieldHeight", fieldHeight);


                    updateOverlayImg();
                    repopulatePosChart();
                    PropWrapper.storeProperties();
                    updateUploadSettings();
                    updateTrajectoryGenerationSettings();
                } catch (IOException e) {
                    Alert alert = AlertFactory.createExceptionAlert(e);

                    alert.showAndWait();
                }
            }
        });
    }

    @FXML
    private void openAboutDialog() {
        Dialog<Boolean> aboutDialog = DialogFactory.createAboutDialog();

        aboutDialog.showAndWait();
    }

    @FXML
    private String showExportDialog() {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(workingDirectory);
        fileChooser.setTitle("Export");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Comma Separated Values", "*.csv"),
                new FileChooser.ExtensionFilter("Binary Trajectory File", "*.traj")
        );

        File result = fileChooser.showSaveDialog(root.getScene().getWindow());
        String parentPath = "";
        if (result != null && generateTrajectories()) {
            parentPath = result.getAbsolutePath();
            String ext = parentPath.substring(parentPath.lastIndexOf("."));
            parentPath = parentPath.substring(0, parentPath.lastIndexOf(ext));

            String csvTypeStr = properties.getProperty("ui.csvType", "0");
            int csvType = Integer.parseInt(csvTypeStr);

            try {
                if (csvType == 0) {
                    backend.exportTrajectoriesJaci(new File(parentPath), ext);
                    parentPath += "_traj.csv";
                } else {
                    backend.exportTrajectoriesTalon(new File(parentPath), ext);
                }
            } catch (Pathfinder.GenerationException e) {
                Alert alert = AlertFactory.createExceptionAlert(e, "Invalid Trajectory!");
                alert.showAndWait();
            } catch (IOException | Pathfinder.OutputException e) {
                e.printStackTrace();
            }
            return parentPath;
        }
        return null;
    }

    @FXML
    private void showUploadFileDialog() {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(workingDirectory);
        fileChooser.setTitle("Upload");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Comma Separated Values", "*.csv"));
        File result = fileChooser.showOpenDialog(root.getScene().getWindow());

        upload(result.getAbsolutePath());
    }

    @FXML
    private void ExportAndUpload() {
        upload(showExportDialog());
    }

    @FXML
    private void showSaveAsDialog() {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(workingDirectory);
        fileChooser.setTitle("Save As");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Extensive Markup Language", "*.xml")
        );

        File result = fileChooser.showSaveDialog(root.getScene().getWindow());

        if (result != null)
            try {
                workingDirectory = result.getParentFile();

                backend.saveProjectAs(result);

                enableMenuParts();
            } catch (Exception e) {
                Alert alert = AlertFactory.createExceptionAlert(e);

                alert.showAndWait();
            }
    }

    @FXML
    private void showOpenDialog() {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(workingDirectory);
        fileChooser.setTitle("Open Project");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Extensive Markup Language", "*.xml")
        );

        File result = fileChooser.showOpenDialog(root.getScene().getWindow());

        if (result != null) {
            try {
                workingDirectory = result.getParentFile();
                backend.loadProject(result);

                updateFrontend();
                updateChartAxis();

                generateTrajectories();

                enableMenuParts();
                disablequickupload();
            } catch (Exception e) {
                Alert alert = AlertFactory.createExceptionAlert(e);

                alert.showAndWait();
            }
        }
    }

    @FXML
    private void showImportDialog() {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(workingDirectory);
        fileChooser.setTitle("Import");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Vannaka Properties File", "*.bot")
        );

        File result = fileChooser.showOpenDialog(root.getScene().getWindow());

        if (result != null) {
            Dialog<ProfileGenerator.Units> unitsSelector = new Dialog<>();
            Optional<ProfileGenerator.Units> unitsResult = null;
            GridPane grid = new GridPane();
            ToggleGroup radGroup = new ToggleGroup();
            RadioButton
                    radImperial = new RadioButton("Imperial (ft)"),
                    radMetric = new RadioButton("Metric (m)");

            // Reset working directory
            workingDirectory = result.getParentFile();

            // Some header stuff
            unitsSelector.setTitle("Select Units");
            unitsSelector.setHeaderText("Select the distance units being used");

            // Some other UI stuff
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));

            grid.add(radImperial, 0, 0);
            grid.add(radMetric, 0, 1);

            radImperial.setToggleGroup(radGroup);
            radImperial.selectedProperty().set(true);
            radMetric.setToggleGroup(radGroup);

            unitsSelector.getDialogPane().setContent(grid);

            // Add all buttons
            unitsSelector.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
            unitsSelector.setResultConverter(buttonType -> {
                if (buttonType.getButtonData() == ButtonBar.ButtonData.OK_DONE) {
                    if (radMetric.selectedProperty().getValue())
                        return ProfileGenerator.Units.METERS;
                    else
                        return ProfileGenerator.Units.FEET;
                }

                return null;
            });

            unitsResult = unitsSelector.showAndWait();

            unitsResult.ifPresent(u -> {
                backend.clearPoints();
                try {
                    backend.importBotFile(result, u);

                    updateFrontend();
                    generateTrajectories();

                    mnuFileSave.setDisable(!backend.hasWorkingProject());
                } catch (Exception e) {
                    Alert alert = AlertFactory.createExceptionAlert(e);

                    alert.showAndWait();
                }
            });
        }
    }

    @FXML
    private void save() {
        updateBackend();

        try {
            backend.saveWorkingProject();
        } catch (Exception e) {
            Alert alert = AlertFactory.createExceptionAlert(e);

            alert.showAndWait();
        }
    }

    @FXML
    private void addPointOnClick(MouseEvent event) {
        boolean addWaypointOnClick = Boolean.parseBoolean(
                properties.getProperty("ui.addWaypointOnClick", "false")
        );

        if (addWaypointOnClick) {
            // get pixel location
            Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());
            double xLocal = axisPosX.sceneToLocal(mouseSceneCoords).getX();
            double yLocal = axisPosY.sceneToLocal(mouseSceneCoords).getY();

            // get location in units (ft, m, in)
            double raw_x = axisPosX.getValueForDisplay(xLocal).doubleValue();
            double raw_y = axisPosY.getValueForDisplay(yLocal).doubleValue();

            // round location
            double rnd_x;
            double rnd_y;

            if (backend.getUnits() == ProfileGenerator.Units.FEET) {
                rnd_x = Mathf.round(raw_x, 0.5);
                rnd_y = Mathf.round(raw_y, 0.5);
            } else if (backend.getUnits() == ProfileGenerator.Units.METERS) {
                rnd_x = Mathf.round(raw_x, 0.25);
                rnd_y = Mathf.round(raw_y, 0.25);
            } else if (backend.getUnits() == ProfileGenerator.Units.INCHES) {
                rnd_x = Mathf.round(raw_x, 6.0);
                rnd_y = Mathf.round(raw_y, 6.0);
            } else {
                rnd_x = Mathf.round(raw_x, 2);
                rnd_y = Mathf.round(raw_y, 2);
            }


            if (rnd_x >= axisPosX.getLowerBound() && rnd_x <= axisPosX.getUpperBound() &&
                    rnd_y >= axisPosY.getLowerBound() && rnd_y <= axisPosY.getUpperBound()) {
                if (OSValidator.isMac()) {
                    Optional<Waypoint> result = null;

                    result = DialogFactory.createWaypointDialog(String.valueOf(rnd_x), String.valueOf(rnd_y)).showAndWait();

                    result.ifPresent((Waypoint w) -> waypointsList.add(w));
                } else {
                    Waypoint temp = new Waypoint(rnd_x, rnd_y, 0.0);
                    waypointsList.add(temp);
                }
            }

        } else {
            event.consume();
        }

    }

    @FXML
    private void showAddPointDialog() {
        Dialog<Waypoint> waypointDialog = DialogFactory.createWaypointDialog();
        Optional<Waypoint> result = null;

        // Wait for the result
        result = waypointDialog.showAndWait();

        result.ifPresent((Waypoint w) -> waypointsList.add(w));
    }

    @FXML
    private void showClearPointsDialog(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.setTitle("Clear Points");
        alert.setHeaderText("Clear All Points?");
        alert.setContentText("Are you sure you want to clear all points?");

        Optional<ButtonType> result = alert.showAndWait();

        result.ifPresent((ButtonType t) -> {
            if (t.getButtonData() == ButtonBar.ButtonData.OK_DONE)
                waypointsList.clear();
        });
    }

    @FXML
    private void resetData() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.setTitle("Create New Project?");
        alert.setHeaderText("Confirm Reset");
        alert.setContentText("Are you sure you want to reset all data? Have you saved?");

        Optional<ButtonType> result = alert.showAndWait();

        result.ifPresent((ButtonType t) -> {
            if (t == ButtonType.OK) {
                backend.clearWorkingFiles();
                backend.resetValues(choUnits.getSelectionModel().getSelectedItem().toUpperCase());

                updateFrontend();
                waypointsList.clear();
                updateChartAxis();

                disableMenuParts();/**/
            }
        });
    }

    @FXML
    private void showSSHKeyDialog(){
        Dialog<Boolean> aboutDialog = DialogFactory.createSSHKeyDialog();
        aboutDialog.showAndWait();
    }

    /** Update from settings*/

    private void updateUploadSettings() {
        this.upload.setTarget(properties.getProperty("ui.targetIP", "10.15.74.2"));
        this.upload.setTargetPath(properties.getProperty("ui.targetPath", "/lvuser/paths"));
    }

    private void updateTrajectoryGenerationSettings() {
        ProfileGenerator.maxNumberOfPoints = Integer.parseInt(properties.getProperty("ui.maxPoints", "1000"));
    }

    /** Update frontend and backend*/
    //TODO: if only one waypoint - draw it
    @FXML
    private void changeTheme() {
        //mainScene is public and static in the Main class so we can reach it by Main.mainScene
        String darkModeFile = Paths.darkMode;
        boolean darkIsOn = Main.mainScene.getStylesheets().contains(getClass().getResource(darkModeFile).toExternalForm());
        if (darkIsOn)
            Main.mainScene.getStylesheets().removeAll(getClass().getResource(darkModeFile).toExternalForm());
        else
            Main.mainScene.getStylesheets().add(getClass().getResource(darkModeFile).toExternalForm());

        mnuTheme.setText("change Theme to " + (!darkIsOn ? "Normal" : "Dark"));
    }

    @FXML
    private void updateBackend() {
        backend.setTimeStep(Double.parseDouble(txtTimeStep.getText().trim()));
        backend.setAcceleration(Double.parseDouble(txtAcceleration.getText().trim()));
        backend.setWheelBaseW(Double.parseDouble(txtWheelBaseW.getText().trim()));
        backend.setWheelBaseD(Double.parseDouble(txtWheelBaseD.getText().trim()));
        backend.setlookaheadDist(Double.parseDouble(txtLookaheadDist.getText().trim()));
        backend.setStartVelocity(Double.parseDouble(txtStartVelocity.getText().trim()));
        backend.setEndVelocity(Double.parseDouble(txtEndVelocity.getText().trim()));
    }

    @FXML
    private void deletePoints() {
        List<Integer> selectedIndicies = tblWaypoints.getSelectionModel().getSelectedIndices();

        int firstIndex = selectedIndicies.get(0);
        int lastIndex = selectedIndicies.get(selectedIndicies.size() - 1);

        waypointsList.remove(firstIndex, lastIndex + 1);
    }

    private void disableMenuParts() {
        disablequickupload();
        mnuFileSave.setDisable(true);
    }

    private void enableMenuParts() {
        mnuFileSave.setDisable(false);
    }

    private void updateFrontend() {
        txtTimeStep.setText("" + backend.getTimeStep());
        txtAcceleration.setText("" + backend.getAcceleration());
        txtWheelBaseW.setText("" + backend.getWheelBaseW());
        txtWheelBaseD.setText("" + backend.getWheelBaseD());
        txtLookaheadDist.setText("" + backend.getLookaheadDist());
        txtEndVelocity.setText(""+backend.getEndVelocity());
        txtStartVelocity.setText(""+backend.getStartVelocity());

        choDriveBase.setValue(choDriveBase.getItems().get(backend.getDriveBase().ordinal()));
        choFitMethod.setValue(choFitMethod.getItems().get(backend.getFitMethod().ordinal()));
        choUnits.setValue(choUnits.getItems().get(backend.getUnits().ordinal()));


        refreshWaypointTable();
    }

    private void updateDriveBase(ObservableValue<? extends String> observable, Object oldValue, Object newValue) {
        String choice = ((String) newValue).toUpperCase();
        ProfileGenerator.DriveBase db = ProfileGenerator.DriveBase.valueOf(choice);

        backend.setDriveBase(db);

        txtWheelBaseD.setDisable(db == ProfileGenerator.DriveBase.TANK);
        lblWheelBaseD.setDisable(db == ProfileGenerator.DriveBase.TANK);

        generateTrajectories();
    }

    private void updateFitMethod(ObservableValue<? extends String> observable, Object oldValue, Object newValue) {
        String choice = ((String) newValue).toUpperCase();
        Trajectory.FitMethod fm = Trajectory.FitMethod.valueOf("HERMITE_" + choice);

        backend.setFitMethod(fm);

        generateTrajectories();
    }

    private void updateUnits(ObservableValue<? extends String> observable, Object oldValue, Object newValue) {
        String new_str = ((String) newValue).toUpperCase();
        String old_str = ((String) oldValue).toUpperCase();
        ProfileGenerator.Units new_unit = ProfileGenerator.Units.valueOf(new_str);
        ProfileGenerator.Units old_unit = ProfileGenerator.Units.valueOf(old_str);

        backend.setUnits(new_unit);

        backend.updateVarUnits(old_unit, new_unit);

        updateChartAxis();
        updateFrontend();
    }

    private void updateOverlayImg() {
        String dir = properties.getProperty("ui.overlayDir", "");

        if (!dir.isEmpty()) {
            try {
                File img = new File(dir);
                chtPosition.lookup(".chart-plot-background").setStyle(
                        "-fx-background-image: url(" + img.toURI().toString() + ");" +
                                "-fx-background-size: stretch;" +
                                "-fx-background-position: top right;" +
                                "-fx-background-repeat: no-repeat;"
                );
            } catch (Exception e) {
                Alert alert = AlertFactory.createExceptionAlert(e);

                alert.showAndWait();
            }
        }
    }

    private void repopulatePosChart() {

        updateChartAxis();

        // Clear data from position graph
        chtPosition.getData().clear();

        // Start by drawing drive train trajectories
        if (waypointsList.size() > 1) {
            XYChart.Series<Double, Double>
                    flSeries = SeriesFactory.buildPositionSeries(backend.getFrontLeftTrajectory()),
                    frSeries = SeriesFactory.buildPositionSeries(backend.getFrontRightTrajectory());

            if (backend.getDriveBase() == ProfileGenerator.DriveBase.SWERVE) {
                XYChart.Series<Double, Double>
                        blSeries = SeriesFactory.buildPositionSeries(backend.getBackLeftTrajectory()),
                        brSeries = SeriesFactory.buildPositionSeries(backend.getBackRightTrajectory());

                chtPosition.getData().addAll(blSeries, brSeries, flSeries, frSeries);
                flSeries.getNode().setStyle("-fx-stroke: red");
                frSeries.getNode().setStyle("-fx-stroke: red");
                blSeries.getNode().setStyle("-fx-stroke: blue");
                brSeries.getNode().setStyle("-fx-stroke: blue");

                for (XYChart.Data<Double, Double> data : blSeries.getData())
                    data.getNode().setVisible(false);

                for (XYChart.Data<Double, Double> data : brSeries.getData())
                    data.getNode().setVisible(false);
            } else {
                chtPosition.getData().addAll(flSeries, frSeries);

                flSeries.getNode().setStyle("-fx-stroke: rgba(255,12,25,0.45)");
                frSeries.getNode().setStyle("-fx-stroke: rgba(255,12,25,0.45)");
            }

            for (XYChart.Data<Double, Double> data : flSeries.getData())
                data.getNode().setVisible(false);

            for (XYChart.Data<Double, Double> data : frSeries.getData())
                data.getNode().setVisible(false);
        }

        String srcDisplayStr = properties.getProperty("ui.sourceDisplay", "2");
        int sourceDisplay = Integer.parseInt(srcDisplayStr);

        // Draw source (center) trajectory and waypoints on top of everything
        if (!waypointsList.isEmpty() && sourceDisplay > 0) {

            if (waypointsList.size() > 1 && sourceDisplay == 2) {
                XYChart.Series<Double, Double> sourceSeries =
                        SeriesFactory.buildPositionSeries(backend.getSourceTrajectory());
                chtPosition.getData().add(sourceSeries);
                sourceSeries.getNode().setStyle("-fx-stroke: orange");

                for (XYChart.Data<Double, Double> data : sourceSeries.getData())
                    data.getNode().setVisible(false);
            }

            XYChart.Series<Double, Double> waypointSeries;
            waypointSeries = SeriesFactory.buildWaypointsSeries(waypointsList.toArray(new Waypoint[1]));

            chtPosition.getData().add(waypointSeries);
            waypointSeries.getNode().setStyle("-fx-stroke: transparent");
            for (XYChart.Data<Double, Double> data : waypointSeries.getData())
                data.getNode().setStyle("-fx-background-color: orange, white");
        }
    }

    private void repopulateVelChart() {
        // Clear data from velocity graph
        chtVelocity.getData().clear();

        if (waypointsList.size() > 1) {
            XYChart.Series<Double, Double>
                    flSeries = SeriesFactory.buildVelocitySeries(backend.getFrontLeftTrajectory()),
                    frSeries = SeriesFactory.buildVelocitySeries(backend.getFrontRightTrajectory()),
                    cruiseSeries = SeriesFactory.buildVelocitySeries(backend.getSourceTrajectory());

            chtVelocity.getData().addAll(cruiseSeries, flSeries, frSeries);
            for (XYChart.Data<Double, Double> data : flSeries.getData())
                data.getNode().setVisible(false);
            for (XYChart.Data<Double, Double> data : frSeries.getData())
                data.getNode().setVisible(false);
            for (XYChart.Data<Double, Double> data : cruiseSeries.getData())
                data.getNode().setVisible(false);
            if (backend.getDriveBase() == ProfileGenerator.DriveBase.SWERVE) {
                XYChart.Series<Double, Double>
                        blSeries = SeriesFactory.buildVelocitySeries(backend.getBackLeftTrajectory()),
                        brSeries = SeriesFactory.buildVelocitySeries(backend.getBackRightTrajectory());

                chtVelocity.getData().addAll(flSeries, frSeries, blSeries, brSeries);

                flSeries.setName("Front Left Trajectory");
                frSeries.setName("Front Right Trajectory");
                blSeries.setName("Back Left Trajectory");
                brSeries.setName("Back Right Trajectory");
                cruiseSeries.setName("Cruise Trajectory");
            } else {
                flSeries.getNode().setStyle("-fx-stroke: rgb(151, 241, 102,0.3);");
                frSeries.getNode().setStyle("-fx-stroke: rgb(241, 87, 39, 0.3);");
                flSeries.setName("Left Trajectory");
                frSeries.setName("Right Trajectory");
                cruiseSeries.setName("Cruise Trajectory");
            }


            XYChart.Series<Double, Double> waypointVelSeries;
            waypointVelSeries = SeriesFactory.buildWaypointsVelSeries(waypointsList.toArray(new Waypoint[1]), backend.getSourceTrajectory());
            waypointVelSeries.setName("Waypoints");

            chtVelocity.getData().add(waypointVelSeries);
            waypointVelSeries.getNode().setStyle("-fx-stroke: transparent");
            for (XYChart.Data<Double, Double> data : waypointVelSeries.getData())
                data.getNode().setStyle("-fx-background-color: #1628ff, white");
        }
    }

    public void repopulateTrajList() {
        Trajectory traj = backend.getSourceTrajectory();
        try {
            segments.clear();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i < traj.segments.length; i++) {
            segments.add(traj.segments[i]);
        }
    }

    private void cleanCharts() {
        chtVelocity.getData().clear();
        chtPosition.getData().clear();
        tblTrajectory.getItems().clear();
    }

    private void updateChartAxis() {

        final double fieldWidth = Double.parseDouble(properties.getProperty("ui.fieldWidth", "12"));
        final double fieldHeight = Double.parseDouble(properties.getProperty("ui.fieldHeight", "12"));

        switch (backend.getUnits()) {
            case FEET:
                axisPosX.setUpperBound(meterToFeet(fieldWidth));
                axisPosX.setTickUnit(1);
                axisPosX.setLabel("X-Position (ft)");
                axisPosY.setUpperBound(meterToFeet(fieldHeight));
                axisPosY.setTickUnit(1);
                axisPosY.setLabel("Y-Position (ft)");

                axisVel.setLabel("Velocity (ft/s)");

                break;
            case METERS:
                axisPosX.setUpperBound(fieldWidth);
                axisPosX.setTickUnit(0.5);
                axisPosX.setLabel("X-Position (m)");
                axisPosY.setUpperBound(fieldHeight);
                axisPosY.setTickUnit(0.5);
                axisPosY.setLabel("Y-Position (m)");

                axisVel.setLabel("Velocity (m/s)");

                break;
            case INCHES:
                axisPosX.setUpperBound(meterToInch(fieldWidth));
                axisPosX.setTickUnit(12);
                axisPosX.setLabel("X-Position (in)");
                axisPosY.setUpperBound(meterToInch(fieldHeight));
                axisPosY.setTickUnit(12);
                axisPosY.setLabel("Y-Position (in)");

                axisVel.setLabel("Velocity (in/s)");
                break;
            default:
                backend.setUnits(ProfileGenerator.Units.METERS);
                updateChartAxis();
        }
    }

    public void refreshWaypointTable() {
        /**
         * Refreshes the waypoints table by clearing the waypoint list and repopulating it.
         * Bad way to update the waypoint list...
         * However, TableView.refresh() is apparently borked?
         */
        List<Waypoint> tmp = new ArrayList<>(backend.getWaypointsList());
        try {
            waypointsList.clear();
            waypointsList.addAll(tmp);
        } catch (NullPointerException e) {
        }
    }

    /** Generate*/
    private boolean generateTrajectories() {
        updateBackend();

        if (waypointsList.size() > 1) {
            try {
                backend.updateTrajectories();
                //if there is a output exception the profile can still be shown to the user
                repopulatePosChart();
                repopulateVelChart();
                repopulateTrajList();
            } catch (Pathfinder.GenerationException e) {
                Toolkit.getDefaultToolkit().beep();

                Alert alert = new Alert(Alert.AlertType.WARNING);

                alert.setTitle("Invalid Trajectory");
                alert.setHeaderText("Invalid trajectory point!");
                alert.setContentText("The trajectory is invalid\n" +
                        "make sure all the waypoints are as expected. \n" +
                        "error message:" + e.getMessage());
                alert.showAndWait();

                cleanCharts();
            } catch (Pathfinder.OutputException e) {
                Toolkit.getDefaultToolkit().beep();

                Alert alert = new Alert(Alert.AlertType.WARNING);

                alert.setTitle("Invalid Output");
                alert.setHeaderText("Invalid trajectory output!");
                alert.setContentText("make sure to check the trajectory before running it\n" +
                        "error message:" + e.getMessage());
                alert.showAndWait();

                repopulatePosChart();
                repopulateVelChart();
                repopulateTrajList();
            }
        }
        return true;
    }

    /** Suppurt*/
    @FXML
    private void exit() {
        System.exit(0);
    }

    @FXML
    private void validateFieldEdit(ActionEvent event) {
        String val = ((TextField) event.getSource()).getText().trim();
        double d = 0;
        boolean validInput = true;

        try {
            d = Double.parseDouble(val);
            validInput = d >= 0;
        } catch (NumberFormatException e) {
            validInput = false;
        } finally {
            if (validInput) {
                generateTrajectories();
            }
            else
                Toolkit.getDefaultToolkit().beep();
        }
    }

    public static boolean ThemeIsOn(String themePath){
        return Main.mainScene.getStylesheets().contains(MainUIController.class.getResource(themePath).toExternalForm());
    }

    /** upload - support*/
    @FXML
    private void quickUpload() {
        if (upload.getQuickUpload() != null) upload(upload.getQuickUpload());
        else disablequickupload();
    }

    private void upload(String path) {
        if (path == null) return;
        Alert alert;
        try {
            alert = AlertFactory.uploadSuccessAlert(this.upload.uploadToTarget(path));
        } catch (Exception e) {
            alert = AlertFactory.createExceptionAlert(e, "Upload failed! " + e.getMessage());
        }
        alert.showAndWait();
        enableQuickUpload();//this might be inside the try
    }

    private void disablequickupload() {
        upload.resetQuickUpload();
        mnuQuickUpload.setText("Quick Upload");
        mnuQuickUpload.setDisable(true);
    }

    private void enableQuickUpload() {
        if (upload.getQuickUploadName() != null) {
            mnuQuickUpload.setText(String.format("Quick Upload current file as %s", upload.getQuickUploadName()));
            mnuQuickUpload.setDisable(false);
        } else disablequickupload();
    }
}

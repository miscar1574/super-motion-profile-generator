package com.mammen.ssh;

import java.io.File;

public class Path {

    private String path; //the path

    /*create a new path*/
    public Path(String path){
        this.path = path;
    }

    /*copy an other path*/
    public Path(Path path){
        this.path = path.path;
    }

    /*get file name with it type*/
    public String getShortName(String fileType){
        File temp = new File(path);
        String name = temp.getName();
        if(name.contains(fileType)) return name;
        return name + fileType;
    }

    /*get the file type*/
    public String getFileType(){
        int index = path.lastIndexOf('.');
        if (index > 0) return path.substring(index+1);
        return null;
    }

    /*get the full path with it type*/
    public String getFullPath(String defaultFileType){
        if(getFileType() != null) return path;
        return path + defaultFileType;
    }

    /*add a file name, assume your path is directory before*/
    public void addFileName(String name){
        path += "/"+name;
    }

    /*generate a string*/
    public String toString() {
        return path;
    }
}

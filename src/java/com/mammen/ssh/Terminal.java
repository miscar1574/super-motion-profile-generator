package com.mammen.ssh;

import com.mammen.ssh.output.TerminalOutput;
import com.mammen.ssh.output.TerminalError;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.LinkedList;
import java.util.Queue;

public class Terminal {

    //run a command on terminal and get it output and errors as Queue
    protected static Queue<TerminalOutput> runCmd(String cmd){
        String line;
        Queue<TerminalOutput> output = new LinkedList<>();

        try {
            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            // read the output from the command
            while ((line = stdInput.readLine()) != null) {
               output.add(new TerminalOutput(line));
            }
            // read any errors from the attempted command
            while ((line = stdError.readLine()) != null) {
                output.add(new TerminalError(line));
            }
        }
        catch (Exception e) {
            System.out.println("exception happened");
            e.printStackTrace();
        }
        finally {
            return output;

        }
    }

    //check if an output queue contain errors
    public static boolean finishedWithError(Queue<TerminalOutput> output){
        for (TerminalOutput tmp : output)
            if(tmp instanceof TerminalError) return true;
        return false;
    }

    //get all the errors in queue as a string
    public static String getErrorAsString(Queue<TerminalOutput> output){
        String error = "";
        for (TerminalOutput tmp : output)
            if(tmp instanceof TerminalError) error += tmp.getOutput() + "\n";
        return error;
    }
}

package com.mammen.ssh;

import com.mammen.ssh.output.TerminalOutput;
import java.net.InetAddress;
import java.util.Queue;

public class ssh extends Terminal {

    //TODO: check on windows machine

    private boolean connectionIsValid = false;  //target is valid and responded to ping
    private String username;                    //the user name to connect to
    private String ip;                          //the ip address of the target

    /*establish and check a new connection*/
    public void establishConnection(String ip, String user) throws sshException {
        //TO-DO: add a check for the connection
        if(TargetIsReachable(ip)) {
            this.username = user;
            this.ip = ip;
            connectionIsValid = true;
        }
        else throw new sshException("target is not reachable \ncheck the target IP in settings and make sure the connection is valid");
    }

    /*close the current connection*/
    public void closeConnection() throws sshException{
        if(connectionIsValid){
            username = null;
            ip = null;
            connectionIsValid = false;
        }
        else throw new sshException("failed to close connection since there is no connection to close");
    }

    /*run a ssh command on target*/
    public Queue<TerminalOutput> sshCommand(String cmd) throws sshException {
        return super.runCmd(generateFullSshCmd(cmd));
    }

    /*run an scp command from local to target*/
    public Queue<TerminalOutput> scpCommand(String localPath, String remotePath) throws sshException {
        Queue<TerminalOutput> output = super.runCmd(generateFullScpCmd(localPath, remotePath));
        if(Terminal.finishedWithError(output)) throw new sshException("scp to target failed \nscp output: "+Terminal.getErrorAsString(output));
        return output;
    }

    /*generate a full ssh command to run on server*/
    private String generateFullSshCmd(String cmd) throws sshException {
        if(connectionIsValid) return String.format("ssh %s@%s %s",username, ip, cmd);
        throw new sshException("no open connection");
    }

    /*generate a full scp command*/
    private String generateFullScpCmd(String localPath, String remotePath) throws sshException {
        if(connectionIsValid) return String.format("scp %s %s@%s:%s",localPath,username, ip, remotePath);
        throw new sshException("no open connection");
    }

    /*check if target is responding to ping from local computer*/
    private static boolean TargetIsReachable(String ipString){
        try {
            //convert string ip to byte array
            byte[] byteIp = new byte[4];
            for (int i = 0; i < byteIp.length; i++) {
                byteIp[i] = (byte)(Integer.parseInt(ipString.substring(0, (ipString.indexOf('.')==-1 ? ipString.length():ipString.indexOf('.')))));
                ipString = ipString.substring(ipString.indexOf('.')+1);
            }
            InetAddress address = InetAddress.getByAddress(byteIp);
            return address.isReachable(450);
        }
        catch (Exception e){
            return false;
        }
    }

    /*thrown when an error occurred during the ssh or scp operation */
    public static class sshException extends Exception{
        public sshException(String message) {
            super(message);
        }
    }
}



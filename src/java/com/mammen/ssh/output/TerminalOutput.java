package com.mammen.ssh.output;

public class TerminalOutput{

    protected String output;

    public TerminalOutput(String output){
        this.output = output;
    }

    public String getOutput(){
        return output;
    }
}

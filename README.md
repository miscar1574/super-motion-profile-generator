Super Motion Profile Generator
=
you may read the CHANGELOG.md file for more information about the current status of this project  
#Project Dependencies:
before running the project you must install the next dependencies

Java - 11.0.6
-
if not already installed, install [java 11.0.6](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)\
when installed:
- go to `File` -> `Project Structre` -> `Project`
- make sure the project SDK is selected to the right version of java
- you may add `java 11.0.6` as a new SDK using the `New...` button 

javaFX - 11.0.2
-
- install [javaFX](https://gluonhq.com/products/javafx) or use the preinstalled javaFX library for macOS on `/super-motion-profile-generator/dependencies/javafx-sdk-11.0.2/lib`
- add the JavaFX package to the project libraries
    - go to `File` -> `Project Structre` -> `Libraries` 
    - press the `+` button
    - select Java and open the javaFX `lib` package
    - add the module to the `super-motion-profile-generator.main` 
- add the package to the VM settings
    - run to project `Run` -> `Run...` - this will auto generate several settings
    - after the project failed to run go to `Run` -> `Edit configurations...`
    - in the `VM options` insert the following text
        ~~~
        module-path PATH_TO_JavaFX_LIB_HERE --add-modules=javafx.controls,javafx.fxml
        ~~~
      make sure you added the path to javaFX in the text for the `VM options`\
      the text might appear in the `VM options` before you paste it, if so you can just change to the correct path

SSH
-
if you want to use the upload options on the app you must first install ssh if it is not preinstalled on your system and also generate a key between your system and your robot\
install ssh:
 - in macOS and most of linux versions ssh is preinstalled, if so you can jump to the generate a key section
 - to install ssh search on the web how to download ssh to your system
 
the instructions for key generation are in the `About` menu


last update: 24.4.2020 - version 4.2.1

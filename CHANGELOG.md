# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [new]
###Added
- added velocity to tableView
- values in tableView are now rounded
- trajectory can start and end at non zero velocities
- segments with small dt compare to the normal dt are now removed from trajectory
- added ssh key generation instructions dialog
- dark mode in dialogs

### Changed
 - rearrange about dialog
 - rearrange settings dialog
 - rearrange add waypoint dialog

### Fixed
- a bug where a user could get stuck in warning messages
- a bug which made the scence not fill the app's winodw until resize
- a bug where empty rows in table view was colored

## [4.2.1] - 2020-4-17
###Fixed
- a bug which made the app to not show the trajectory or alert the user in case of bad point

## [4.2.0] - 2020-4-16
###Added
- Show waypoints on velocity graph.
- improved theme changes
- added better position calculation in trajectory generator
- added a new way to view the trajectory - table view
- added drag&drop to the waypoints list
- added quick upload option

###Fixed
- fixed a bug in the velocity view which made the graph unreadable
- fixed a bug in export dialog
- a bug where a user could get stuck in warning messages
- a bug which caused the last points to be at the same XY position in some cases
- a bug where the app crashed because of too long(over integer/Node limit) trajectory 


## [4.1.0] - 2020-4-7
###Added
- added upload to robot option

## [4.0.0] - 2020-4-5
###Added
- added lookahead support for better workflow
- added dark mode
- organized several classes in the code and added comments

###Fixed
- fixed velocity bug who caused the velocity of the trajectory to be not as expected
- fixed the bad points issue

## [3.0.0] - 2018-4-29
### Added
- Completly rewritten using JavaFX for the GUI
- Added a settings menu with settings that will persist across runs
- Settings now saved in xml in home directory
- Added new unit (Inches)
- Changable background image for position graph.
- Export CSV type; Talon SRX or Jaci
- Show waypoints on position graph.
- Auto regenerage trajectory when any value is changed.
- Other misc changes. Easter eggs?


## [2.3.0] - 2018-2-22
### Added
- Added ability to choose between different units (Feet and Meters)

### Fixed
- Bug where a user could get stuck in point update mode

## [2.2.0] - 2018-2-1
### Added
- Added ability to choose the Fit method of the points. Hermite Cubic or Hermite Quintic
- Added ability to choose between the Tank and Swerve modifiers
- Added Tool tip text

### Fixed
- Bug where app would crash when you double clicked in white space to update a point 

## [2.1.0] - 2018-1-27
### Added 
- Added button to delete last point in the list (Thanks Team 1414)

## [2.0.0] - 2018-1-10
### Added 
- Added graphs for this years game!!!

## [1.2.0] - 2017-12-29
### Added
- Added a preference system to reload old profile settings

### Fixed
- The menu saving option now checks if file exists before saving

## [1.1.0] - 2017-12-20
### Added
- Ability to edit existing waypoints without clearing entire list

## [1.0.2] - 2017-12-16
### Added
- Ability to replace existing file when saving

### Changed
- Moved Velocity Graph to the tabbed pane
- Reworked Saving UX to make it more user friendly
- Motion Profile displays on both red and blue tab

### Fixed
- Resized menu bar to extend across entire window
- Negative variables are now checked for
- Fixed bug that allowed multiple graphs to be displayed on one graph

## [1.0.1] - 2017-11-11
### Added
- Originally only had the blue alliance graph. Added red alliance graph
- Tabbed view to switch between red and blue graphs
- Motion Profile displays based on chosen tab
- Menu bar with various items

### Removed
- Save Button (moved action to menu bar)

## [1.0.0] - 2017-11-05
### Added
- Initial Release
- Graphs to display the motion profile and the velocity
- Text boxes to enter the variables: Time Step, Velocity, Acceleration, Jerk, Wheel Base
- Ability to enter waypoints through a separate text box for each: X, Y, and Angle
- Validation for waypoints
- Display points in a list box each time the "Add Point" button is pressed
- Clear button clears the graphs and the list box 
- Text box to name the output file
- Save button to choose directory and save motion profile
- Generate button to calculate profile and velocity and display them